Creacion del repositorio y anadir companeros
# -DESCRIPCION DEL PROGRAMA
Los administradores de Easy Parking han presentado dificultades en el momento de llevar su control de forma manual o por medio de hojas de calculo, por lo cual, al momento de consolidar informacion diaria, mensual o semestral, generar tiquetes o realizar contabilidad, deben disponer de mucho tiempo para hacer estas labores, retrasando la salida de los vehiculos. Por lo tanto, la necesidad de crear una aplicacion para realizar un sistema es primordial y de esta forma liberar tiempo y obtener datos casi al instante. 
El administrador del parqueadero Easy Parking desea automatizar la operación del negocio, de tal forma que se pueda llevar el *control de plazas disponibles, facturacion y la trazabilidad diaria de ingresos de vehiculos (tipo de vehiculo, color vehiculo, opcional marca).* 

El administrador desea tambien que el usuario pueda contar con un comprobante fisico que le permita el manejo o control de su dinero a fin de conocer la tarifa y la economia que representa usar nuestro easy parking.


