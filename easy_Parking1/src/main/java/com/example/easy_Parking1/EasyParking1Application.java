package com.example.easy_Parking1;

import com.example.easy_Parking1.modelos.Usuarios;
import com.example.easy_Parking1.modelos.Vehiculos;
import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

// PETICIONES HTTP al servidor.
// GET = Consultar 
// POST = Actualizar - Insertar
// PUT = Insertar
// DELETE =  Eliminar
@SpringBootApplication
@RestController
public class EasyParking1Application {
    
    @Autowired (required = true)
    Usuarios u;
    @Autowired(required = true)
    Vehiculos v; 
 
    public static void main(String[] args) {
        SpringApplication.run(EasyParking1Application.class, args);
    }
    
    @GetMapping("/cedula_usuario")
    public String consultarUsuario(@RequestParam(value = "Cedula", defaultValue = "1") String Cedula) throws ClassNotFoundException, SQLException {
        u.setId_Usuarios(Integer.parseInt(Cedula));
        if (u.consultarUsuario()){
            String res = new Gson().toJson(u);
            u.setId_Usuarios(0);
            u.setNombres("");
            u.setApellidos("");
            u.setDireccion("");
            u.setCorreo("");
            u.setCelular(0);
            u.setTipo_Usuario("");
            return res;
        } else {
            return new Gson().toJson(u);
        }
    }
    
    @PostMapping(path ="/cedula_usuario", 
        consumes = MediaType.APPLICATION_JSON_VALUE, // recibe json Nombre= Carlos
        produces = MediaType.APPLICATION_JSON_VALUE)
    
    public String actualizarUsuario(@RequestBody String Usuarios) throws ClassNotFoundException, SQLException{
        Usuarios C = new Gson().fromJson(Usuarios, Usuarios.class);
        u.setId_Usuarios(C.getId_Usuarios());
        u.setCedula(C.getCedula());
        u.setNombres(C.getNombres());
        u.setApellidos(C.getApellidos());
        u.setDireccion(C.getDireccion());
        u.setCorreo(C.getCorreo());
        u.setCelular(C.getCelular());
        u.setTipo_Usuario(C.getTipo_Usuario());
        return new Gson().toJson(u);
    }
    
    @DeleteMapping("/eliminarvehiculo/{placa}")
    public String borrarVehiculo(@PathVariable("Placa") String Placa)throws SQLException, ClassNotFoundException{
        v.setPlaca(Placa);
        v.borrarVehiculo();
        return "Los datos de la placa indicada han sido eliminados";
    } 
    @GetMapping("/vehiculo_Placa")
    public String consultarVehiculo(@RequestParam(value = "Placa", defaultValue = "1") String Placa) throws ClassNotFoundException, SQLException {
    v.setId_Vehiculos(Integer.parseInt("Placa"));
    if(v.consultarVehiculo()){
        String res = new Gson().toJson(u);
        v.setId_Vehiculos(0);
        v.setPlaca("");
        v.setTipo_Vehiculo("");
        v.setHora_Entrada("");
        v.setHora_Salida("");
        return res;
        } else {
            return new Gson().toJson(u);
        }
    }
    
    @PostMapping(path ="/vehiculo_actualizar", 
        consumes = MediaType.APPLICATION_JSON_VALUE, // recibe json Nombre= Carlos
        produces = MediaType.APPLICATION_JSON_VALUE)
     public String actualizarVehiculo(@RequestBody String Vehiculos) throws ClassNotFoundException, SQLException{
        Vehiculos D = new Gson().fromJson(Vehiculos, Vehiculos.class);
        v.setId_Vehiculos(D.getId_Vehiculos());
        v.setPlaca(D.getPlaca());
        v.setTipo_Vehiculo(D.getTipo_Vehiculo());
        v.setValor_Pagado(D.getValor_Pagado());
        v.setHora_Entrada(D.getHora_Entrada());
        v.setHora_Salida(D.getHora_Salida());
        return new Gson().toJson(v);
    }
}
