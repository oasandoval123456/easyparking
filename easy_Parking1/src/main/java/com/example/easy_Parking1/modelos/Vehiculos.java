/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.easy_Parking1.modelos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author familia zarate
 */
@Component("vehiculos")
public class Vehiculos {
    @Autowired
    transient JdbcTemplate jdbcTemplate;
    
    //Atributos 
    private int Id_Vehiculos;
    private String Placa;
    private String Tipo_Vehiculo;
    private float Valor_Pagado;
    private String Hora_Entrada;
    private String Hora_Salida;
    private Usuarios Id_Usuarios; 
    
    //Constructor 

    public Vehiculos(int Id_Vehiculos, String Placa, String Tipo_Vehiculo, float Valor_Pagado, String Hora_Entrada, String Hora_Salida) {
        this.Id_Vehiculos = Id_Vehiculos;
        this.Placa = Placa;
        this.Tipo_Vehiculo = Tipo_Vehiculo;
        this.Valor_Pagado = Valor_Pagado;
        this.Hora_Entrada = Hora_Entrada;
        this.Hora_Salida = Hora_Salida;
    }

    //Metodos

    public int getId_Vehiculos() {
        return Id_Vehiculos;
    }

    public void setId_Vehiculos(int Id_Vehiculos) {
        this.Id_Vehiculos = Id_Vehiculos;
    }

    public String getPlaca() {
        return Placa;
    }

    public void setPlaca(String Placa) {
        this.Placa = Placa;
    }

    public String getTipo_Vehiculo() {
        return Tipo_Vehiculo;
    }

    public void setTipo_Vehiculo(String Tipo_Vehiculo) {
        this.Tipo_Vehiculo = Tipo_Vehiculo;
    }

    public float getValor_Pagado() {
        return Valor_Pagado;
    }

    public void setValor_Pagado(float Valor_Pagado) {
        this.Valor_Pagado = Valor_Pagado;
    }

    public String getHora_Entrada() {
        return Hora_Entrada;
    }

    public void setHora_Entrada(String Hora_Entrada) {
        this.Hora_Entrada = Hora_Entrada;
    }

    public String getHora_Salida() {
        return Hora_Salida;
    }

    public void setHora_Salida(String Hora_Salida) {
        this.Hora_Salida = Hora_Salida;
    }

    public Usuarios getId_Usuarios() {
        return Id_Usuarios;
    }

    public void setId_Usuarios(Usuarios Id_Usuarios) {
        this.Id_Usuarios = Id_Usuarios;
    }

    @Override
    public String toString() {
        return "Vehiculos{" + "Id_Vehiculos=" + Id_Vehiculos + ", Placa=" + Placa + ", Tipo_Vehiculo=" + Tipo_Vehiculo + ", Valor_Pagado=" + Valor_Pagado + ", Hora_Entrada=" + Hora_Entrada + ", Hora_Salida=" + Hora_Salida + ", Id_Usuarios=" + Id_Usuarios + '}';
    }

    public Vehiculos() {
    }
    
    
    //Guardar pago
    public void registrarPago() throws ClassNotFoundException, SQLException{
        String sql = "INSERT INTO transaccion(Valor_Pagado) VALUES(?)";
        jdbcTemplate.execute(sql);
    }
    
    //CRUD
    //Guardar
    public void guardarVehiculo() throws ClassNotFoundException, SQLException{
        String sql = "INSTER into vehiculos (Id_Vehiculos, Placa, Tipo_Vehiculo, Valor_Pagado, Hora_Entrada, Hora_Salida) VALUES(?, ?, ?, ?, ?, ?)";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setString(2, this.getPlaca());
        ps.setString(3, this.getTipo_Vehiculo());
        ps.setFloat(4, this.getValor_Pagado());
        ps.setString(5, this.getHora_Entrada());
        ps.setString(6, this.getHora_Salida());
        ps.executeUpdate();
        ps.close();
    }
    
    //Consultar 
    public boolean consultarVehiculo() throws ClassNotFoundException, SQLException{
        String sql = "SELECT Id_Vehiculos, Placa, Tipo_Vehiculo, Valor_Pagado, Estado FROM vehiculos WHERE Id_Usuarios = ?";
        List<Vehiculos> vehiculo = jdbcTemplate.query(sql, (rs, rowNum)
                    -> new Vehiculos (
                                rs.getInt("Id_Vehiculos"),
                                rs.getString("Placa"),
                                rs.getString("Tipo_Vehiculo"),
                                rs.getFloat("Valor_Pagado"),
                                rs.getString("Hora_Entrada"),
                                rs.getString("Hora_Salida")
                    ), new Object []{this.getId_Usuarios()});
        if (vehiculo != null &&  !vehiculo.isEmpty()) {
            this.setId_Vehiculos(vehiculo.get(0).getId_Vehiculos());
            this.setPlaca(vehiculo.get(0).getPlaca());
            this.setTipo_Vehiculo(vehiculo.get(0).getTipo_Vehiculo());
            this.setValor_Pagado(vehiculo.get(0).getValor_Pagado());
            this.setHora_Entrada(vehiculo.get(0).getHora_Salida());
            this.setHora_Salida(vehiculo.get(0).getHora_Salida());
            
            return true;
        } else{
            return false;
        }
    }
    
    public List<Vehiculos> consultarTodoVehiculo(int Id_Usuarios) throws ClassNotFoundException, SQLException{
        String sql = "SELECT Id_Vehiculos, Placa, Tipo_Vehiculo, Valor_Pagado, Hora_Entrada, Hora_Salida FROM vehiculos WHERE Id_Usuarios = ?";
        List<Vehiculos> vehiculo = jdbcTemplate.query(sql, (rs, rowNum)
                    -> new Vehiculos (
                                rs.getInt("Id_Vehiculos"),
                                rs.getString("Placa"),
                                rs.getString("Tipo_Vehiculo"),
                                rs.getFloat("Valor_Pagado"),
                                rs.getString("Hora_Entrada"),
                                rs.getString("Hora_Salida")
                    ), new Object []{this.getId_Usuarios()});
        
        return vehiculo;
    }
    
    //CRUD Update
    public void actualizarVehiculo() throws ClassNotFoundException, SQLException{
        String sql = "Update vehiculos SET Placa = ?, Tipo_Vehiculo = ?, Valor_Pagado = ?, Hora_Entrada = ?, Hora_Salida = ? WHERE Id_Vehiculos = ?";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setString(2, this.getPlaca());
        ps.setString(3, this.getTipo_Vehiculo());
        ps.setFloat(4, this.getValor_Pagado());
        ps.setString(5, this.getHora_Entrada());
        ps.setString(6, this.getHora_Salida());
    }
    
    //CRUD Borrar
    public boolean borrarVehiculo() throws ClassNotFoundException, SQLException{
        String sql = "DELETE FROM vehiculos WHERE Id_Vehiculos = ?";
        Connection d = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = d.prepareStatement(sql);
        ps.setInt(1, this.getId_Vehiculos());
        ps.execute();
        ps.close();
        
        return true;
    }
    
    
}
