/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.easy_Parking1.modelos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author familia zarate
 */
@Component("usuarios")
public class Usuarios {
    
    @Autowired
    transient JdbcTemplate jdbcTemplate;
    
    //Atributos
    private int Id_Usuarios;
    private int Cedula;
    private String Nombres;
    private String Apellidos;
    private String Direccion;
    private String Correo;
    private int Celular;
    private String Tipo_Usuario;
    
    //Constructor 

    public Usuarios(int Id_Usuarios, int Cedula, String Nombres, String Apellidos, String Direccion, String Correo, int Celular, String Tipo_Usuario) {
        this.Id_Usuarios = Id_Usuarios;
        this.Cedula = Cedula;
        this.Nombres = Nombres;
        this.Apellidos = Apellidos;
        this.Direccion = Direccion;
        this.Correo = Correo;
        this.Celular = Celular;
        this.Tipo_Usuario = Tipo_Usuario;
    }
    
    //Metodos

    public int getId_Usuarios() {
        return Id_Usuarios;
    }

    public void setId_Usuarios(int Id_Usuarios) {
        this.Id_Usuarios = Id_Usuarios;
    }

    public int getCedula() {
        return Cedula;
    }

    public void setCedula(int Cedula) {
        this.Cedula = Cedula;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String Correo) {
        this.Correo = Correo;
    }

    public int getCelular() {
        return Celular;
    }

    public void setCelular(int Celular) {
        this.Celular = Celular;
    }

    public String getTipo_Usuario() {
        return Tipo_Usuario;
    }

    public void setTipo_Usuario(String Tipo_Usuario) {
        this.Tipo_Usuario = Tipo_Usuario;
    }

    public Usuarios() {
    }
    

    @Override
    public String toString() {
        return "Usuarios{" + "Id_Usuarios=" + Id_Usuarios + ", Cedula=" + Cedula + ", Nombres=" + Nombres + ", Apellidos=" + Apellidos + ", Direccion=" + Direccion + ", Correo=" + Correo + ", Celular=" + Celular + ", Tipo_Usuario=" + Tipo_Usuario + '}';
    }
   
    public boolean consultarUsuario() throws ClassNotFoundException, SQLException{
        String sql = "Select Id_Usuarios, Cedula, Nombres, Apellidos, Direccion, Correo, Celular, Tipo_Usuario FROM usuarios WHERE Id_Usuarios = ?";
        List<Usuarios> usuario = jdbcTemplate.query(sql, (rs, rowNum)
                        -> new Usuarios(
                                rs.getInt("Id_Usuarios"),
                                rs.getInt("Cedula"),
                                rs.getString("Nombres"),
                                rs.getString("Apellidos"),
                                rs.getString("Direccion"),
                                rs.getString("Correo"),
                                rs.getInt("Celular"),
                                rs.getString("Tipo_Vehiculo")
                        ), new Object []{this.getId_Usuarios()});
        if (usuario != null && !usuario.isEmpty()){
            this.setId_Usuarios(usuario.get(0).getId_Usuarios());
            this.setCedula(usuario.get(0).getCedula());
            this.setNombres(usuario.get(0).getNombres());
            this.setApellidos(usuario.get(0).getApellidos());
            this.setDireccion(usuario.get(0).getDireccion());
            this.setCelular(usuario.get(0).getCelular());
            this.setTipo_Usuario(usuario.get(0).getTipo_Usuario());
            return true;
        } else{
            return false;
        }
    }
    
    public void actualizarUsuario() throws ClassNotFoundException, SQLException{
        String sql = "UPDATE usuarios SET Cedula = ?, Nombres = ?, Apellidos = ?, Direccion = ?, Correo = ?, Celular = ?, Tipo_Usuario = ? WHERE Id_Usuarios = ?"; 
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setInt(2, this.getCedula());
        ps.setString(3, this.getNombres());
        ps.setString(4, this.getApellidos());
        ps.setString(5, this.getDireccion());
        ps.setString(6, this.getCorreo());
        ps.setInt(7, this.getCelular());
        ps.setString(8, this.getTipo_Usuario());
        ps.close();
    }
    
    
}
