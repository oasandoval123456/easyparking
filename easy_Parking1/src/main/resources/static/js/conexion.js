/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */

//JavaScript source code
angular.module('easy_Parking1', [])
        .controller('filasVehiculos', function ($scope, $http) {

            $scope.Usuarios = "";
            $scope.Vehiculos = "";
            $scope.Costo = "";

            // accion del boton consultar
            $scope.consultarVehiculo = function () {
                if ($scope.Vehiculos === undefined || $scope.Vehiculos === null) {
                    $scope.Vehiculos = 0;
                }

                $http.get("/easyparking1/vehiculos?Placa=" + $scope.Vehiculos).then(function (data) {
                    console.log(data.data);
                    $scope.Usuarios = data.data.usuario;
                    $scope.Vehiculos = data.data.vehiculos;
                    $scope.Costo = data.data.pagos;
                }, function () {
                    //error
                    $scope.Usuarios = "";
                    $scope.Vehiculos = "";
                    $scope.Costo = "";
                    $scope.filas = []; // guarda
                });

                $http.get("/Usuarios?Vehiculos=" + $scope.Vehiculos).then(function (data) {
                    console.log(data.data);
                    $scope.filas = data.data;
                }, function () {
                    $scope.filas = [];
                });
            };

            //acción del botón actualizar
            $scope.actualizar = function () {
                if ($scope.Vehiculos === undefined || $scope.Vehiculos === null) {
                    $scope.Vehiculos = 0;
                }
                data = {
                    "Id_Usuarios": $scope.Usuarios,
                    "Vehiculos": $scope.Vehiculos,
                    "pagos": $scope.pagos
                };
                $http.post('/vehiculos', data).then(function (data) {
                    //success
                    $scope.Usuarios = data.data.usuario;
                    $scope.Vehiculos = data.data.vehiculos;
                    $scope.Costo = data.data.pagos;
                }, function () {
                    //error
                    $scope.Usuarios = "";
                    $scope.Vehiculos = "";
                    $scope.Costo = "";
                    $scope.filas = [];
                });

            };
            // acción del botón borrar
            $scope.borrarVehiculo = function () {
                if ($scope.Id_Vehiculos === undefined || $scope.Id_Vehiculos === null) {
                    $scope.Id_Vehiculos = 0;
                }
                data = {
                    "id": $scope.idcuenta
                };
                $http.delete('/eliminarcuenta/' + $scope.idcuenta, data).then(function (data) {
                    alert("La cuenta ha sido eliminada");

                }, function () {
                    alert("Ha ocurrido un error");
                });
            };
        });