/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.EasyParking.modelos;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.GenerationType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author familia zarate
 */

@Component("tickets")
public class Tickets {
    
    @Autowired
    transient JdbcTemplate jdbcTemplate; // parquete que nos permite ejecutar las consultas sql
    //Puede que no siempre se ejecute la consulta. 
    
   //Atributos
    private int Id_Tickets;
    private String Fecha;
    private String Hora_Entrada;
    private String Hora_Salida;
    private Usuarios Id_Usuarios; //Llave foranea
    private Vehiculos Placa; //Llave foranea
    private Estacionamientos Id_Estacionamiento; //Llave foranea
    private Tarifa Id_Tarifa; //Llave foranea 
   
   //Contructor

    public Tickets(int Id_Tickets, String Fecha, String Hora_Entrada, String Hora_Salida) {
        super(); //Hereda de la clase principal
        this.Id_Tickets = Id_Tickets;
        this.Fecha = Fecha;
        this.Hora_Entrada = Hora_Entrada;
        this.Hora_Salida = Hora_Salida;
    }
    
    //Metodos - GET & SET y otros 
    //GET --> Mostrar o consultar // SET --> Modificar
    public int getId_Tickets() {
        return Id_Tickets;
    }

    public void setId_Tickets(int Id_Tickets) {
        this.Id_Tickets = Id_Tickets;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String Fecha) {
        this.Fecha = Fecha;
    }

    public String getHora_Entrada() {
        return Hora_Entrada;
    }

    public void setHora_Entrada(String Hora_Entrada) {
        this.Hora_Entrada = Hora_Entrada;
    }

    public String getHora_Salida() {
        return Hora_Salida;
    }

    public void setHora_Salida(String Hora_Salida) {
        this.Hora_Salida = Hora_Salida;
    }

    public int getId_Usuarios() {
        return Id_Usuarios;
    }

    public void setId_Usuarios(int Id_Usuarios) {
        this.Id_Usuarios = Id_Usuarios;
    }

    public String getPlaca() {
        return Placa;
    }

    public void setPlaca(String Placa) {
        this.Placa = Placa;
    }

    public int getId_Estacionamiento() {
        return Id_Estacionamiento;
    }

    public void setId_Estacionamiento(int Id_Estacionamiento) {
        this.Id_Estacionamiento = Id_Estacionamiento;
    }

    public int getId_Tarifa() {
        return Id_Tarifa;
    }

    public void setId_Tarifa(int Id_Tarifa) {
        this.Id_Tarifa = Id_Tarifa;
    }

    @Override
    public String toString() {
        return "Tickets{" + "Id_Tickets=" + Id_Tickets + ", Fecha=" + Fecha + ", Hora_Entrada=" + Hora_Entrada + ", Hora_Salida=" + Hora_Salida + ", Id_Usuarios=" + Id_Usuarios + ", Placa=" + Placa + ", Id_Estacionamiento=" + Id_Estacionamiento + ", Id_Tarifa=" + Id_Tarifa + '}';
    }

    
    // CRUD - C (Create) --> Guardar
    
    public String guardar(){
        String sql = "INSERT INTO tickets(Id_Tickets, Fecha, Hora_Entrada, Hora_Salida) VALUES(?, ?, ?, ?)";
        return sql;
    }
    
    //CRUD - R (Read) --> Consultar 
    
    public boolean consultar() throws ClassNotFoundException, SQLException{
        String sql = "SELECT tickets, Fecha, Hora_Entrada, Hora_Salida FROM tickets WHERE Id_Usuarios = ?";
        // rs = apuntador
        List<Tickets> ticket = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Tickets(
                        rs.getInt("Id_Tickets"),
                        rs.getString("Fecha"),
                        rs.getString("Hora_Entrada"),
                        rs.getString("Hora_Salida")
                ), new Object []{this.getId_Usuarios()});
        if (ticket != null && !ticket.isEmpty()){
            this.setId_Tickets(ticket.get(0).getId_Tickets());
            this.setFecha(ticket.get(0).getFecha());
            this.setHora_Entrada(ticket.get(0).getHora_Entrada());
            this.setHora_Salida(ticket.get(0).getHora_Salida());
            
            return true;
        }
        else {
            return false; 
        }
    }
    //CRUD - R (Read) --> Consultar con string
     public List<Tickets> consultarUsuario(int Id_Usuarios) throws ClassNotFoundException, SQLException{
         String sql = "SELECT tickets, Fecha, Hora_Entrada, Hora_Salida FROM Id_Tickets WHERE Id_Usuarios = ?";
        // rs = apuntador
        List<Tickets> ticket = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Tickets(
                        rs.getInt("Id_Tickets"),
                        rs.getString("Fecha"),
                        rs.getString("Hora_Entrada"),
                        rs.getString("Hora_Salida")
                ), new Object []{this.getId_Usuarios()});
        return ticket;
     }
     
     // CRUD - U (Update)
     public String actualizar()throws ClassNotFoundException, SQLException{
         String sql = "UPDATE tickets SET Fecha = ?, Hora_Entrada = ?, Hora_Salida = ? WHERE Id_Tickets = ?";
         return sql;
     }
     
     //CRUD - D (Delete) 
     public boolean borrar() throws ClassNotFoundException, SQLException{
         String sql = "DELETE FROM tickets WHERE Id_Tickets = ?";
         Connection c = jdbcTemplate.getDataSource().getConnection();
         PreparedStatement ps = c.prepareStatement(sql);
         ps.setInt(1, this.getId_Tickets());
         ps.execute();
         ps.close();
         
         return true;
     }

}
