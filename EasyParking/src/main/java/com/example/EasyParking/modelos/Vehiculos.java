/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.EasyParking.modelos;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author familia zarate
 */

@Entity
@Table(name="vehiculos")
public class Vehiculos implements Serializable {
    
    @Autowired
    transient JdbcTemplate jdbcTemplate;
    
    //Atributos
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Placa")
    private String Placa;
    @Column(name = "Tipo_Vehiculo")
    private String Tipo_Vehiculo;
    @OneToOne
    @JoinColumn(name = "Id_Usuario")
    private int Id_Usuario;
    @OneToOne
    @JoinColumn(name = "Id_Estacionamiento")
    private int Id_Estacionamiento; 
    
    //Constructor

    public Vehiculos(String Placa, String Tipo_Vehiculo) {
        this.Placa = Placa;
        this.Tipo_Vehiculo = Tipo_Vehiculo;
    }
    
    //Metodos

    public String getPlaca() {
        return Placa;
    }

    public void setPlaca(String Placa) {
        this.Placa = Placa;
    }

    public String getTipo_Vehiculo() {
        return Tipo_Vehiculo;
    }

    public void setTipo_Vehiculo(String Tipo_Vehiculo) {
        this.Tipo_Vehiculo = Tipo_Vehiculo;
    }

    public int getId_Usuario() {
        return Id_Usuario;
    }

    public void setId_Usuario(int Id_Usuario) {
        this.Id_Usuario = Id_Usuario;
    }

    public int getId_Estacionamiento() {
        return Id_Estacionamiento;
    }

    public void setId_Estacionamiento(int Id_Estacionamiento) {
        this.Id_Estacionamiento = Id_Estacionamiento;
    }

    @Override
    public String toString() {
        return "Vehiculos{" + "Placa=" + Placa + ", Tipo_Vehiculo=" + Tipo_Vehiculo + ", Id_Usuario=" + Id_Usuario + ", Id_Estacionamiento=" + Id_Estacionamiento + '}';
    }
    
    // CRUD - C
    
    public String guardar(){
        String sql = "INSERT INTO vehiculos(Placa, Tipo_Vehiculo) VALUES(?, ?)";
        return sql;
    }
    
    //CRUD - R (Read) --> Consultar 
    
    public boolean consultar() throws ClassNotFoundException, SQLException{
        String sql = "SELECT Placa, Tipo_Vehiculo FROM vehiculos WHERE Id_Usuario = ?";
        // rs = apuntador
        List<Vehiculos> vehiculo = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Vehiculos (
                        rs.getString("Placa"),
                        rs.getString("Tipo_Vehiculo")
                ), new Object []{this.getId_Usuario()});
        if (vehiculo != null && !vehiculo.isEmpty()){
            this.setPlaca(vehiculo.get(0).getPlaca());
            this.setTipo_Vehiculo(vehiculo.get(0).getTipo_Vehiculo());
            return true;
        }
        else {
            return false; 
        }
    }
    
    //CRUD - R (Read) --> Consultar con string
    
    public List<Vehiculos> consultarUsuario(int Id_Usuario) throws ClassNotFoundException, SQLException{
       String sql = "SELECT Placa, Tipo_Vehiculo FROM vehiculos WHERE Id_Usuario = ?";
        // rs = apuntador
        List<Vehiculos> vehiculo = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Vehiculos (
                        rs.getString("Placa"),
                        rs.getString("Tipo_Vehiculo")
                ), new Object []{this.getId_Usuario()});
        return vehiculo;
    }
    
    //CRUD - D (Delete)
    public boolean borrar() throws ClassNotFoundException, SQLException{
        String sql = "DELETE FROM vehiculos WHERE Placa =?";
        Connection c = jdbcTemplate.getDataSource().getConnection();
         PreparedStatement ps = c.prepareStatement(sql);
         ps.setString(1, this.getPlaca());
         ps.execute();
         ps.close();
         
         return true;
     }
    
    
}
