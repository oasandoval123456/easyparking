package com.example.EasyParking;

import com.example.EasyParking.modelos.Estacionamientos;
import com.example.EasyParking.modelos.Tarifa;
import com.example.EasyParking.modelos.Tickets;
import com.example.EasyParking.modelos.Usuarios;
import com.example.EasyParking.modelos.Vehiculos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class EasyParkingApplication {
    
    @Autowired // Sirve para interpretar una clase sin necesidad de decirle que cree una instancia nueva. Equivalente a c = new Cuenta();
    Usuarios c; // Instancias - Objeto 
    @Autowired
    Vehiculos M; // Instancia
    @Autowired
    Estacionamientos E;
    @Autowired
    Tickets T;        
    @Autowired
    Tarifa P;


	public static void main(String[] args) {
		SpringApplication.run(EasyParkingApplication.class, args);
                
                @GetMapping("/Vehiculos");
	}

}
